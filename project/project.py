import getopt
import sys

from utils import Utils


def main(filename):
    pd_dataframe = Utils().read_csv(filename)


if __name__ == "__main__":
    inputfile, outputfile = (None, None)
    try:
        opts, args = getopt.getopt(sys.argv[1:], "-f:-o:")
    except getopt.GetoptError:
        raise Exception(
            "Usage project.py -f <input_file> -o <output_file>"
        )
    for opt, arg in opts:
        if opt == "-i":
            inputfile = arg
        elif opt == "-o":
            outputfile = arg
    if not inputfile or not outputfile:
        raise Exception(
            "Some arguments were not provided. Usage project.py \
                -f <input_file> -o <output_file>"
        )
    main(inputfile, outputfile)

import pandas as pd


class Utils:
    def read_csv(self, filename):
        return pd.read_csv(filename)

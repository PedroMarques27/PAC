# PAC

The idea of the project is to explore a dataset using Python


## Description  

## Badges  

## Visuals  

## Installation   

If using VS Code for editing, copy and rename the settings_example.json file in the .vscode folder to settings.json (or append the code to preexisting settings.json file) in order to turn on automatic linting and formatting tools.

We use a virtual environment, to initialize it run <code>source venv/bin/activate</code>
To install the required packages run <code>pip install -r requirements.txt</code>

## Getting Started
The Python version used for the project is 3.* POR DECIDIR *  

## Usage  

## Support  

## Roadmap  

## Contributing  
To contribute to the project, you must use the specified style guidelines and tool versions.

During the course of the project we used the [PEP 8](https://peps.python.org/pep-0008/) style guide for writing Python code and its file naming conventions.

Useful links:
- [Python linting in VS Code](https://code.visualstudio.com/docs/python/linting)
- [Flake8](https://flake8.pycqa.org/en/latest/)
- [Setup Black and Isort on VS Code](https://cereblanco.medium.com/setup-black-and-isort-in-vscode-514804590bf9)    

## Authors and acknowledgment

## License  
For open source projects, say how it is licensed.  

## Project status  
In development  


## Links  
- [^1]: https://python-poetry.org/docs/basic-usage/